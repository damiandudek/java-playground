package playground.algorithms.fibonacci;

public class FibonacciOutOfRangeException extends Exception {
    private int fibonacciNumber;

    public FibonacciOutOfRangeException(int fibonacciNumber){
        this.fibonacciNumber = fibonacciNumber;
    }

    @Override
    public String toString(){
        return "FibonacciOutOfRangeException: the number " + fibonacciNumber + " is out of range.";
    }
}
