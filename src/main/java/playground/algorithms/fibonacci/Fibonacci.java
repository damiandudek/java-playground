package playground.algorithms.fibonacci;

public class Fibonacci {
    private static int maxFibonacciNumber = 92;

    public static long iterative(int fibonacciNumberToCalculate) throws FibonacciOutOfRangeException {
        if (fibonacciNumberToCalculate <= 0 || fibonacciNumberToCalculate > maxFibonacciNumber) {
            throw new FibonacciOutOfRangeException(fibonacciNumberToCalculate);
        }
        if (fibonacciNumberToCalculate == 1 || fibonacciNumberToCalculate == 2) {
            return 1;
        }
        long fib = 0;
        long prev = 1;
        long next = 1;
        for (int step = 3; step <= fibonacciNumberToCalculate; step++) {
            fib = prev + next;
            prev = next;
            next = fib;
        }
        return fib;
    }

    public static long recursive(int fibonacciNumberToCalculate) throws FibonacciOutOfRangeException{
        if (fibonacciNumberToCalculate <= 0 || fibonacciNumberToCalculate > maxFibonacciNumber) {
            throw new FibonacciOutOfRangeException(fibonacciNumberToCalculate);
        }
        if (fibonacciNumberToCalculate == 1 || fibonacciNumberToCalculate == 2) {
            return 1;
        }
        return recursive(fibonacciNumberToCalculate - 1) + recursive(fibonacciNumberToCalculate - 2);
    }
}
