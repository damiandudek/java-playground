package playground.algorithms;

/**
 * Task: check whether a given number is a prime number.
 */
public class PrimeNumber {

    /**
     * A prime number is a natural number greater than 1 that cannot be formed by multiplying two
     * smaller natural numbers.
     * @param number to check.
     * @return true if given number is prime, false otherwise.
     */
    public static boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        }
        if (number > 2 && number % 2 == 0) {
            return false;
        }

        double limit = Math.sqrt(number);

        for (int i = 3; i < limit; i+=2) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
