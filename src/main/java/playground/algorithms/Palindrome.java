package playground.algorithms;

/**
 * Verify whether the given String is a palindrome.
 * A string is a palindrome if it remains unchanged when reversed.
 */
public class Palindrome {
    //Comparisons: n/2

    public static boolean isPalindrome(String text) {
        text = text.toLowerCase();
        int length = text.length();
        for (int i = 0; i < length / 2; i++) {
            if (text.charAt(i) != text.charAt(length - i - 1)) {
                return false;
            }
        }
        return true;
    }
}
