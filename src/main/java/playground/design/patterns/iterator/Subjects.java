package playground.design.patterns.iterator;

interface Subjects {
    Iterator createIterator();
}
