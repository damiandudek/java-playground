package playground.design.patterns.singleton;

/**
 * A thread safe implementation of lazy load singleton. Low performance - only one thread at a
 * time.
 */
final class SynchronizedSingleton {
    private static SynchronizedSingleton instance;

    // the constructor is private to prevent the use of "new"
    private SynchronizedSingleton() {
    }

    public static synchronized SynchronizedSingleton getInstance() {
        // Lazy initialization
        if (instance == null) {
            instance = new SynchronizedSingleton();
            System.out.print("Creates a new instance of object");
        } else {
            System.out.print("You already have created an instance of object.");
        }
        return instance;
    }
}