package playground.design.patterns.singleton;

/**
 * Java ensures that any enum value is instantiated only once in a Java program.
 * Doesn't allow lazy load.
 */
public enum EnumSingleton {
    INSTANCE;

    public static void singletonMethod() {
        System.out.println("Singleton method.");
    }

    public static int add(int a, int b) {
        return a + b;
    }
}
