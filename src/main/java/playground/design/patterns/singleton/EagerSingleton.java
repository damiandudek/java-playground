package playground.design.patterns.singleton;

/**
 * Singleton with eager initialization. Disadvantages: an instance is created before it is needed
 * (might not be needed at all). There are no options for exception handling (if anything goes wrong
 * while initializing).
 */
public class EagerSingleton {

  private static final EagerSingleton instance = new EagerSingleton();

  private EagerSingleton() {
  }

  public static EagerSingleton getInstance() {
    return instance;
  }

}
