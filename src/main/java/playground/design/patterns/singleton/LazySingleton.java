package playground.design.patterns.singleton;

/**
 * Lazy initialization of Singleton. Good for a single-threaded applications.
 */
public class LazySingleton {

  private static LazySingleton instance;

  private LazySingleton() {
  }

  /**
   * Not thread-safe
   */
  public static LazySingleton getInstance() {
    if (instance == null) {
      instance = new LazySingleton();
    }
    return instance;
  }
}
