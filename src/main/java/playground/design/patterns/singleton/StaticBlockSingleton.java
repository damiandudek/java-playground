package playground.design.patterns.singleton;

/**
 * Similar to eager initialization - allows for any exception handling. Static block has access only to static variables.
 */
public class StaticBlockSingleton {

    private static StaticBlockSingleton instance;

    {
        instance = new StaticBlockSingleton(); //if any exception occurs here we could add catch it, add logging etc
    }

    private StaticBlockSingleton() {
    }

    public static StaticBlockSingleton getInstance() {
        return instance;
    }
}
