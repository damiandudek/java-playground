package playground.design.patterns.singleton;

/**
 * Thread safe implementation of lazy initialization Singleton. Has better performance than
 * synchronized singleton due to using double checking and a volatile read.
 */
public class VolatileSingleton {

    private static volatile VolatileSingleton instance;

    private VolatileSingleton() {
    }

    public static VolatileSingleton getInstance() {
        if (instance == null) { //first check, only then synchronizing
            synchronized (VolatileSingleton.class) {
                if (instance == null) { //second check, after obtaining lock on class
                    instance = new VolatileSingleton();
                }
            }
        }
        return instance;
    }
}
