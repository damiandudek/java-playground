package playground.design.patterns.factory;

interface Animal {
    void speak();

    void preferredAction();
}
