package playground.design.patterns.composite;

interface IEmployee {
    void printStructures();
    int getEmployeeCount();
}
