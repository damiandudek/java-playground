package playground.design.patterns.proxy;

// Abstract class Subject
abstract class Subject {
    public abstract void doSomeWork();
}