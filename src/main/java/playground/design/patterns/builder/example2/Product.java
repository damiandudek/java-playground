package playground.design.patterns.builder.example2;

import java.util.LinkedList;

class Product {
    // Any data structure is allowed.

    private LinkedList<String> parts;

    public Product() {
        parts = new LinkedList<String>();
    }

    public void add(String part) {
        parts.addLast(part);
    }

    public void showProduct() {
        System.out.println("\nProduct completed as below :");
        for (String part : parts)
            System.out.println(part);
    }
}