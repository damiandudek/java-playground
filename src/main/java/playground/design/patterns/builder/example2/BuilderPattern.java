package playground.design.patterns.builder.example2;

/**
 * Separate the construction of a complex object from its representation so that the same construction processes can create different representations.
 * The builder pattern is useful for creating complex objects that have multiple parts.
 * The creational mechanism of an object should be independent of these parts.
 * The construction process does not care about how these parts are assembled.
 * The same construction process must allow us to create different representations of the objects.
 */
public class BuilderPattern {
}
