package playground.design.patterns.adapter;

class Calculator {
    public double getArea(Rectangle rect) {
        return rect.length * rect.width;
    }
}