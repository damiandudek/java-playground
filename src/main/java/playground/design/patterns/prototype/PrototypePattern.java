package playground.design.patterns.prototype;

/**
 * Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.
 * Creating a new instance from scratch is a costly operation. Using the prototype pattern, new instances are created
 * by copying or cloning an instance of an existing one. This approach saves both time and money in creating a new instance from scratch.
 */
public class PrototypePattern {

}
