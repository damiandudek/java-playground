package playground.design.patterns.flyweight;

public interface Robot {
    //Color comes from client.It is extrinsic.
    void showMe(String color);
}
