package playground.design.patterns.observer;

interface Observer {
    void update(int updatedValue);
}