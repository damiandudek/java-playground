package playground.design.patterns.observer;

interface SubjectInterface {
    void register(Observer anObserver);

    void unregister(Observer anObserver);

    void notifyRegisteredUsers(int notifiedValue);
}