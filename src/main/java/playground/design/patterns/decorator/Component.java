package playground.design.patterns.decorator;

abstract class Component {
    public abstract void makeHouse();
}
