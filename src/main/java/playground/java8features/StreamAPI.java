package playground.java8features;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamAPI {
    public static void main(String[] args) {

        List<Integer> integers = new ArrayList<>(Arrays.asList(1, 5, 7, 9, 6, 4, 40, 72, 1, 47, 1, 33, 43, 1));
        List<String> languages = new ArrayList<>();
        languages.add("french");
        languages.add("German");
        languages.add("spanish");
        languages.add("English");
        languages.add("polish");

        System.out.println("\nLanguages to upper case:" + StreamAPI.toUpperCase(languages));
        System.out.println("\nSorted languages list: " + StreamAPI.sortList(languages));
        System.out.println("\nReversed languages list: " + StreamAPI.getReversedStringList(languages));
        System.out.println("\nLanguages consisted of 6 characters: "
            + StreamAPI.findElementsWithConcreteLength(languages, 6));
        System.out.println("\nLanguages which start with 'E' letter: "
            + StreamAPI.findElementsStartsWithString(languages, "E"));
        System.out.println("\nSum of all numbers from list: " + StreamAPI.getSum(integers));
        System.out.println("\nAverage is: " + StreamAPI.average(integers));
        System.out.println("\nMaximum number in list: " + StreamAPI.getMaximumFromList(integers));
        System.out.println("\nIs number 47 present in list: " + StreamAPI.isNumberPresentInList(47, integers));
        System.out.println(
            "\nHow many times number 1 occurred in list: " + StreamAPI.getNumberOccurrencesInList(1, integers));
        System.out.println("\nDigits in list: " + StreamAPI.getDigits(integers));
        System.out.println(
            "\nDistinct and ordered digits from list: " + StreamAPI.getDistinctOrderedDigits(integers));
    }

    public static List<String> toUpperCase(List<String> list) {
        return list.stream().map(String::toUpperCase).collect(Collectors.toList());
    }

    private static List<String> sortList(List<String> languages) {
        return languages.stream().sorted().collect(Collectors.toList());
    }

    public static List<String> findElementsStartsWithString(List<String> list, String startWithString) {
        return list.stream().filter(s -> s.startsWith(startWithString)).collect(Collectors.toList());
    }

    public static List<String> findElementsWithConcreteLength(List<String> list, int length) {
        return list.stream().filter(s -> s.length() == length).collect(Collectors.toList());
    }

    public static Double average(List<Integer> list) {
        return list.stream().mapToInt(i -> i).average().getAsDouble();
    }

    public static Integer getSum(List<Integer> list) {
        return list.stream().mapToInt(i -> i).sum();
    }

    public static Integer getMaximumFromList(List<Integer> list) {
        return list.stream().max(Comparator.comparing(Integer::valueOf)).get();
    }

    public static List<String> getReversedStringList(List<String> list) {
        return list.stream().map(s -> new StringBuilder(s).reverse().toString()).collect(Collectors.toList());
    }

    public static boolean isNumberPresentInList(Integer n, List<Integer> list) {
        return list.stream().anyMatch(i -> i == n ? true : false);
    }

    public static Integer getNumberOccurrencesInList(Integer n, List<Integer> list) {
        return (int) list.stream().filter(i -> i.equals(n)).count();
    }

    public static List<Integer> getDigits(List<Integer> list) {
        return list.stream().filter(i -> (i < 10)).collect(Collectors.toList());
    }

    public static List<Integer> getDistinctOrderedDigits(List<Integer> list) {
        return list.stream().filter(i -> (i < 10)).distinct().sorted().collect(Collectors.toList());
    }
}