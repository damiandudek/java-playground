package playground.algorithms.fibonacci;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FibonacciRecursiveTest {

    @Test(expected = FibonacciOutOfRangeException.class)
    public void givenInvalidInputReturnsFibonacciOutOfRangeException() throws FibonacciOutOfRangeException {
        int givenNumber = -5;
        Fibonacci.recursive(givenNumber);
    }

    @Test(expected = FibonacciOutOfRangeException.class)
    public void givenOutOfMaxRangeInputReturnsFibonacciOutOfRangeException() throws FibonacciOutOfRangeException {
        int givenNumber = 93;
        Fibonacci.recursive(givenNumber);
    }

    @Test
    public void givenValidInputReturnsNthFibonacciNumber() throws FibonacciOutOfRangeException {
        long expected = 1;
        int givenNumber = 2;
        assertEquals(expected, Fibonacci.recursive(givenNumber));

        expected = 13;
        givenNumber = 7;
        assertEquals(expected, Fibonacci.iterative(givenNumber));

        expected = 832040;
        givenNumber = 30;
        assertEquals(expected, Fibonacci.iterative(givenNumber));
    }
}
