package playground.algorithms.fibonacci;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FibonacciIterativeTest {

    @Test(expected = FibonacciOutOfRangeException.class)
    public void givenInvalidInputReturnsFibonacciOutOfRangeException() throws FibonacciOutOfRangeException {
        int givenNumber = -5;
        Fibonacci.iterative(givenNumber);
    }

    @Test(expected = FibonacciOutOfRangeException.class)
    public void givenOutOfMaxRangeInputReturnsFibonacciOutOfRangeException() throws FibonacciOutOfRangeException {
        int givenNumber = 93;
        Fibonacci.iterative(givenNumber);
    }

    @Test
    public void givenValidInputReturnsNthFibonacciNumber() throws FibonacciOutOfRangeException {
        long expected = 1;
        int givenNumber = 1;
        assertEquals(expected, Fibonacci.iterative(givenNumber));

        expected = 7540113804746346429L;
        givenNumber = 92;
        assertEquals(expected, Fibonacci.iterative(givenNumber));

        expected = 144;
        givenNumber = 12;
        assertEquals(expected, Fibonacci.iterative(givenNumber));
    }
}
