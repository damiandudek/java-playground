package playground.algorithms;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PalindromeTest {

    @Test
    public void givenPalindromeShouldReturnTrue() {
        String palindrome = "kajak";
        assertTrue(Palindrome.isPalindrome(palindrome));
        palindrome = "TacoCat";
        assertTrue(Palindrome.isPalindrome(palindrome));
    }

    @Test
    public void givenNotPalindromeShouldReturnFalse() {
        String text = "not a palindrome";
        assertFalse(Palindrome.isPalindrome(text));
        text = "taco cat";
        assertFalse(Palindrome.isPalindrome(text));
    }
}
