package playground.algorithms;

import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class PrimeNumberTest {

    @Test
    public void givenPrimeShouldReturnTrue() {
        int number = 2;
        assertTrue(PrimeNumber.isPrime(number));
        number = 7;
        assertTrue(PrimeNumber.isPrime(number));
        number = 43;
        assertTrue(PrimeNumber.isPrime(number));
        number = 97;
        assertTrue(PrimeNumber.isPrime(number));
    }

    @Test
    public void givenNotPrimeShouldReturnFalse() {
        int number = -7;
        assertFalse(PrimeNumber.isPrime(number));
        number = 0;
        assertFalse(PrimeNumber.isPrime(number));
        number = 1;
        assertFalse(PrimeNumber.isPrime(number));
        number = 48;
        assertFalse(PrimeNumber.isPrime(number));
    }
}