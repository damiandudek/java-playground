package playground.design.patterns.prototype;

import org.junit.Test;

public class PrototypePatternTest {
    @Test
    public void testPrototypePattern() throws CloneNotSupportedException {
        BasicCar nano = new Nano("Green Nano");
        nano.basePrice = 100000;

        BasicCar ford = new Ford("Ford Yellow");
        ford.basePrice = 500000;

        BasicCar bc1;
        bc1 = nano.clone();

        //Price will be more than 100000 for sure
        bc1.onRoadPrice = nano.basePrice + BasicCar.setAdditionalPrice();
        System.out.println("Car is: " + bc1.modelName + " and it's price is Rs." + bc1.onRoadPrice);

        bc1 = ford.clone();
        //Price will be more than 500000 for sure
        bc1.onRoadPrice = ford.basePrice + BasicCar.setAdditionalPrice();
        System.out.println("Car is: " + bc1.modelName + " and it's price is Rs." + bc1.onRoadPrice);
    }
}
