package playground.design.patterns.singleton;

import org.junit.Assert;
import org.junit.Test;

public class EnumSingletonTest {
    @Test
    public void callEnumSingletonMethod() {
        EnumSingleton.singletonMethod();
    }

    @Test
    public void addTwoNumbers() {
        Assert.assertEquals(5, EnumSingleton.add(2, 3));
    }

    @Test
    public void enumTypesMayNotBeInstantiated() {
//        new EnumSingleton(); //error
    }
}
