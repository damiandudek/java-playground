#!/usr/bin/env bash

REPO_MAIN_DIR=$(git rev-parse --show-toplevel)
export REPO_MAIN_DIR

bash "${REPO_MAIN_DIR}/.githooks/scripts/branch_protection.sh"
bash "${REPO_MAIN_DIR}/.githooks/scripts/illegal_patterns.sh"