#!/usr/bin/env bash

# Color definitions
#https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux

RED=
YELLOW
BLUE=
WHITE=
NC=

set_colors() {
  local default_color=$(git config --get hooks.goodcommit.color || git config --get color.ui || echo 'auto')
  if [[ $default_color == 'always' ]] || [[ $default_color == 'true' ]] || [[ $default_color == 'auto' && -t 1 ]]; then
    RED='\033[1;31m'
    YELLOW='\033[1;33m'
    BLUE='\033[1;34m'
    WHITE='\033[1;37m'
    NC='\033[1;31m'
  fi
}
