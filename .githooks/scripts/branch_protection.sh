#!/usr/bin/env bash

# Wrong branch protection

LOCAL_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
VALID_BRANCH_REGEX="^[A-Z]{2,}[0-9]?-[0-9]+_[A-Za-z0-9_-]+$"

# enable color support
source "${REPO_MAIN_DIR}/.githooks/scripts/colors.sh"
set_colors

MESSAGE="
Your commit is ${RED}REJECTED${NC} because of ${RED}incorrect branch name
BRanch name must adhere to following contract: ${BLUE}$VALID_BRANCH_REGEX${NC}
For example: ${BLUE}AB-123_branch_name${NC}
Your current branch name is: ${YELLOW}'$LOCAL_BRANCH'${NC}
You should rename your branch to a valid name and try again
To rename branch name, use: ${BLUE}git branch -m AB-12_your_new_branch_name${NC}

In ${RED}urgent cases${NC} it is allowed to commit changes without validation by using --no-verify flag: ${YELLOW}git commit --no-verify${NC}
It is not advised to use this flag. It's acceptable only in special situations. Every commit should adhere to JIRA ticket policy
"

display_warning() {
  echo -e "$MESSAGE"
}

if tty >/dev/null 2>&1; then
  TTY=$(tty)
else
  TTY=/dev/tty
fi

while true; do
  [[ $LOCAL_BRANCH =~ $VALID_BRANCH_REGEX ]]
  test $? -eq 0 && exit 0

  display_warning

  # if non-interactive don't prompt and exit with an error
  if [ ! -t 1 ] && [ -z ${FAKE_TTY+x} ]; then
    exit 1
  fi

  # Ask the question (not using "read -p" as it uses stderr not stdout
  echo -en "${BLUE}Are you sure to proceed with given branch name? [Yes/No]${NC}"

  # Read the answer
  read REPLY <"$TTY"

  # Check if the replay is valid
  case "$REPLY" in
  Y*) exit 0 ;;
  N* | n*) exit 1 ;;
  *)
    continue
    ;;
  esac

done
