#!/usr/bin/env bash

# The wrong import protection
PATTERN_LIST="${REPO_MAIN_DIR}/.githooks/scripts/illegal_patterns.txt"

# enable color support
source "${REPO_MAIN_DIR}/.githooks/scripts/colors.sh"
set_colors

git diff --cached --name-status --diff-filter=ACM | cut -d$'\t' -f2 | while read st file; do
  diffstr=$(cat "${st}" | grep -E -f "$PATTERN_LIST")
  diffstr=$(git -c core.pager= diff --cached --diff-filter=ACM "${st}" | grep "^\+" | cut -c2- | grep -n -E -f "${PATTERN_LIST}")
  if [ "$diffstr" != "" ] ; then
    line_no=$(echo "${diffstr}" | cut -d':' -f 1)
    illegal_line=$(echo "${diffstr}" | cut -d':' -f 2-)
    echo -e "${RED}ERROR${NC}: disallowed line detected, please fix this:"
    echo
    echo -e "${YELLOW}${st}:${line_no}${NC}:${RED}${illegal_line}${NC}"
    echo
    echo -e "If this is intentional, skip the hook verification by proper parameter to '${YELLOW}git commit${NC}'"
    exit 1
  fi
done
